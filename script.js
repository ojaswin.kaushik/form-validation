
// Write a program that shows a form on web browser, 
//with fields email, password, sex (drop down with option of male, female, other), 
//Role ( Radio button with option of Admin and User), Permissions (Checkbox with option Perm1, Perm2, Perm3, Perm4),
// Submit button.
// On clicking Submit button, following validation should take place
// All fields should be filled.
// Email should be valid
// password should be min 6 character with MIX of Uppercase, lowercase, digits
// At Least 2 permissions should be ticked.
// If all validation passes, the form and submit button should disappear and all the filled in details in the form should appear with a Confirm Button.

/*
Email :- 
password :-
Sex :- dropdown (M/F/Other)
Role :- radio(Admin/User)
permisssion :- Checkbox
Submit :- Button
*/


// Selectors
const container = document.querySelector('.container')
const user = document.getElementById('username')
const email = document.getElementById('email')
const password = document.getElementById('password')
const gender = document.getElementsByTagName('select')[0]
const admin =document.getElementById('Admin')

const userRole = document.getElementById('User')
const radio = document.querySelectorAll('.radio')
const permisssion = document.querySelectorAll('.permission')
const btn = document.querySelector('.btn')

const Role = document.getElementsByTagName('label')[4]
const PermitL = document.getElementsByTagName('label')[7]
console.log(PermitL);

//Regular Expression
const PassVerify= /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])/;
const EVerify =/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/;


radio.forEach(element => {
    element.addEventListener('click',Toggle)
});

btn.addEventListener('click',Submit)



//Function
function Valid() 
{
    //user
    const name = user.value.split(' ').join('')
    let error=0;
    if (!name.length)
    {
        user.style.border="2px solid red"
        console.log("user error")
        error=1
        
    }
    //email
    if (!EVerify.test(email.value))
    {
        email.style.border="2px solid red"
        console.log("email error",email.value)
        error=1
    }
    //password
    if (!((password.value.length>6) && (PassVerify.test(password.value))))
    
    {
        password.style.border="2px solid red"
        console.log("password error",password.value)
        error=1
        
    }

    // radio

    if (!userRole.checked && !admin.checked)
    {
        Role.style.color="red"
        
        console.log("Role error")
        error=1
    }
    //permission
    let permit =0;
    permisssion.forEach(element => {
        if (element.checked)
            permit+=1;
    });

    if (permit<2)
    {
        PermitL.style.color="red";
        console.log("permit error");
        error=1
    }
    return error
}
function Submit()
{
    
    if (Valid())
    {
        return 0
    }

    const Descript = document.createElement('div');
    const info = document.createElement('h1');
    const ConfirmBtn = document.createElement('button');
    ConfirmBtn.innerText = "Confirm"
    ConfirmBtn.classList.add('btn')
    
    
    Descript.classList.add('describe');
    
    
    let permits=" ";
    permisssion.forEach(element => {
        if (element.checked)
        {
            s+=element.id+" "
            
        }
    });
    info.innerText="Final Confirmation "+"\n" +"\n" +
                        "Name : " + user.value +"\n"+"\n"+
                        "Email : " +email.value + "\n"+"\n"+
                        "Password : " +password.value + "\n"+"\n"+
                        "Sex : " +gender.value + "\n"+"\n"+
                        "Admin: " +(admin.checked?"Admin":"User") + "\n"+"\n"+
                        "Permission : "+ permits
                        
    info.style.fontSize="18px"

    
                        
    
    container.appendChild(Descript)
    Descript.appendChild(info);
    Descript.appendChild(ConfirmBtn)

    document.getElementsByTagName('section')[0].style.display="none"

}
function Toggle()
{
    if (this.id==="Admin") 
        
    {
        userRole.checked=false;
    }
    else if (this.id==="User")
    {
        admin.checked=false;
    }
    
}